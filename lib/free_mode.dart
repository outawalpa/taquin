import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';

import 'package:taquin/free_mode_list.dart';
import 'package:taquin/const/monuments.dart';
import 'package:taquin/const/animals.dart';

class FreeMode extends StatefulWidget {
  const FreeMode({super.key});

  @override
  State<FreeMode> createState() => _FreeModeState();
}

class _FreeModeState extends State<FreeMode> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blue,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => FreeModeList(list: animals, title: 'Animals',))),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: Image.asset('assets/images/b_animals.jpg'),
                    ),
                    Center(
                      child: BorderedText(
                        strokeWidth: 5,
                        child: Text('Animals'.toUpperCase(),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  ],
                )
            ),
            InkWell(
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => FreeModeList(list: monuments, title: 'Monuments',))),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: Image.asset('assets/images/b_monuments.jpg'),
                    ),
                    Center(
                      child: BorderedText(
                        strokeWidth: 5,
                        child: Text('Monuments'.toUpperCase(),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}