String imageRoot = 'assets/images/';

List<String> storiesBackground = [
  '${imageRoot}bg_animals',
  '${imageRoot}bg_monuments'
];

List<String> storiesTitle = [
  'Discover incredible animals',
  'Around the world'
];