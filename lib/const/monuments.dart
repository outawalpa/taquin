import 'const.dart';

String monumentsRoot = '${imageRoot}monuments/';

List<Map<String, String>> monuments = [
  {
    'image':'${monumentsRoot}eiffel.jpg',
    'name':'La tour Eiffel',
    'description':'La tour Eiffel est une tour de fer puddlé de 330 m2 de hauteur (avec antennes) située à Paris, à l’extrémité nord-ouest du parc du Champ-de-Mars en bordure de la Seine dans le 7e arrondissement.'
  },
  {
    'image':'${monumentsRoot}wall.jpg',
    'name':'La grande muraille de Chine',
    'description':'La Grande Muraille, aussi appelé « Les Grandes Murailles » est un ensemble de fortifications militaires chinoises construites, détruites et reconstruites en plusieurs fois et à plusieurs endroits entre le IIIe siècle av. J.-C. et le XVIIe siècle pour marquer et défendre la frontière nord de la Chine.'
  },
  {
    'image':'${monumentsRoot}kremlin.jpg',
    'name':'Le Kremlin de Moscou',
    'description':'Le kremlin de Moscou, souvent appelé simplement le Kremlina, est un ensemble architectural complexe situé au sein d’une forteresse du centre de Moscou, la capitale de la Russie.'
  },
  {
    'image':'${monumentsRoot}pisa.jpg',
    'name':'La tour de pise',
    'description':'La tour de Pise est le campanile de la cathédrale Notre-Dame de l’Assomption de Pise, en Toscane (Italie).'
  },
  {
    'image':'${monumentsRoot}pyramids.jpg',
    'name':'Les pyramides de Gizeh',
    'description':'Les pyramides de Gizeh, aussi appelées complexe pyramidal de Gizeh, sont l\'ensemble des pyramides égyptiennes situées dans la nécropole de Gizeh sur le plateau de Gizeh.'
  },
];