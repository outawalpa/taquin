import 'const.dart';

String animalsRoot = '${imageRoot}animals/';

List<Map<String, String>> animals = [
  {
    'image':'${animalsRoot}lion.jpg',
    'name':'Lion',
    'description':'Le Lion est une espèce de mammifères carnivores de la famille des Félidés.'
  },
  {
    'image':'${animalsRoot}giraffe.jpg',
    'name':'Giraffe',
    'description':'La Girafe est une espèce de mammifères ongulés artiodactyles, du groupe des ruminants, vivant dans les savanes africaines et répandue du Tchad jusqu\'en Afrique du Sud.'
  },
  {
    'image':'${animalsRoot}frog.jpg',
    'name':'Frog',
    'description':'Le terme grenouille est un nom vernaculaire attribué à certains amphibiens, principalement dans le genre Rana.'
  },
  {
    'image':'${animalsRoot}bear.jpg',
    'name':'Bear',
    'description':'L’ours brun est une espèce d’ours qui peut atteindre une masse corporelle de 130 à 700 kg.'
  },
  {
    'image':'${animalsRoot}snake.jpg',
    'name':'Snake',
    'description':'Les serpents, de nom scientifique Serpentes, forment un sous-ordre de squamates carnivores au corps très allongé et dépourvus de membres apparents.'
  },
];