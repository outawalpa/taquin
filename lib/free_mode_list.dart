import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:taquin/game.dart';

class FreeModeList extends StatefulWidget {
  const FreeModeList({super.key, required this.title, required this.list});

  final String title;
  final List<Map<String, String>> list;

  @override
  State<FreeModeList> createState() => _FreeModeListState();
}

class _FreeModeListState extends State<FreeModeList> {

  int difficulty = 4;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color: Colors.blue,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 25),
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                        onTap: () => Navigator.popAndPushNamed(context, '/freeMode'),
                        child: const Icon(
                          Icons.arrow_back,
                          color: Colors.black,
                          size: 50,
                        )
                    ),
                    BorderedText(
                      strokeWidth: 5,
                      child: Text(widget.title.toUpperCase(),
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const SizedBox(width: 50),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 25),
                alignment: Alignment.center,
                child: Row(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(5),
                      child: BorderedText(
                        strokeWidth: 5,
                        child: Text('Puzzle size : '.toUpperCase(),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(5),
                      child: InkWell(
                        onTap: () => setState(() { difficulty = 4; }),
                        child: BorderedText(
                          strokeWidth: 5,
                          child: Text('4x4',
                            style: TextStyle(
                                color: difficulty == 4 ? Colors.white : Colors.grey,
                                fontSize: difficulty == 4 ? 30 : 20,
                                fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(5),
                      child: InkWell(
                        onTap: () => setState(() { difficulty = 5; }),
                        child: BorderedText(
                          strokeWidth: 5,
                          child: Text('5x5',
                            style: TextStyle(
                                color: difficulty == 5 ? Colors.white : Colors.grey,
                                fontSize: difficulty == 5 ? 30 : 20,
                                fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(5),
                      child: InkWell(
                        onTap: () => setState(() { difficulty = 6; }),
                        child: BorderedText(
                          strokeWidth: 5,
                          child: Text('6x6',
                            style: TextStyle(
                              color: difficulty == 6 ? Colors.white : Colors.grey,
                              fontSize: difficulty == 6 ? 30 : 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(5),
                      child: InkWell(
                        onTap: () => setState(() { difficulty = 7; }),
                        child: BorderedText(
                          strokeWidth: 5,
                          child: Text('7x7',
                            style: TextStyle(
                              color: difficulty == 7 ? Colors.white : Colors.grey,
                              fontSize: difficulty == 7 ? 30 : 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              GridView.count(
                shrinkWrap: true,
                crossAxisCount: 3,
                children: List.generate(widget.list.length, (index) {
                  return Container(
                    padding: const EdgeInsets.all(10),
                    child: Center(
                      child: InkWell(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Game(difficulty: difficulty, imageRoot: widget.list[index]['image']!)));
                          },
                          child: Image.asset(widget.list[index]['image']!)
                      ),
                    ),
                  );
                }),
              ),
            ],
          ),
        )
    );
  }
}

Widget _buildPopupDialog(BuildContext context) {
  return AlertDialog(
    content: Center(
      child: LoadingAnimationWidget.threeRotatingDots(
        color: Colors.white,
        size: 75,
      ),
    ),
  );
}