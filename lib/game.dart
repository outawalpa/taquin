import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart' as imglib;
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';

class Game extends StatefulWidget {
  const Game({super.key, required this.difficulty, required this.imageRoot});

  final int difficulty;
  final String imageRoot;

  @override
  State<Game> createState() => _GameState();
}

class _GameState extends State<Game> {

  bool newGame = true;
  bool finish = false;

  int movements = 0;

  List<TeasingObject> teasingGame = [];
  List<Image> imagesData = [];

  final StopWatchTimer _stopWatchTimer = StopWatchTimer();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() async {
    super.dispose();
    await _stopWatchTimer.dispose();
  }

  Future<bool> callAsyncFetch() async {
    if(imagesData.isEmpty) {
      await createImageList();
      await createGame(widget.difficulty);
      _stopWatchTimer.onStartTimer();
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
        future: callAsyncFetch(),
        builder: (context, AsyncSnapshot<bool> snapshot) {
          //if (imagesData.isNotEmpty) createGame(difficulty);
          return Scaffold(
            body: Container(
              color: Colors.blue,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          const Icon(Icons.directions, color: Colors.white),
                          Text(movements.toString(),
                            style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          const Icon(Icons.access_time, color: Colors.white),
                          StreamBuilder(
                              stream: _stopWatchTimer.rawTime,
                              initialData: 0,
                              builder: (context, snap) {
                                final value = snap.data;
                                final displayTime = StopWatchTimer.getDisplayTime(value!, hours: false, milliSecond: false);
                                return Text(displayTime,
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                );
                              }
                          )
                        ],
                      )
                    ],
                  ),

                  if (teasingGame.length == (widget.difficulty*widget.difficulty+1)) Container(
                    margin: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                        //border: Border.all(color: Colors.white, width: 5)
                    ),
                    child: GridView.count(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      crossAxisCount: widget.difficulty,
                      children: List.generate((widget.difficulty * widget.difficulty), (index) {
                        return Center(
                          child: InkWell(
                              onTap: () { selectImage(index); },
                              child: teasingGame[index].image
                          ),
                        );
                      }),
                    ),
                  ),
                  if (teasingGame.isEmpty || teasingGame.length != (widget.difficulty*widget.difficulty+1)) Center(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white, width: 5)
                      ),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.width,
                      child: Center(
                        child: LoadingAnimationWidget.threeRotatingDots(
                          color: Colors.white,
                          size: 75,
                        ),
                      ),
                    ),
                  ),

                  IconButton(
                    iconSize: 50,
                    icon: const Icon(Icons.flag),
                    color: Colors.white,
                    onPressed: () async {
                      Navigator.popAndPushNamed(context, '/menu');
                    },
                  ),
                ],
              ),
            )
          );
        });
  }

  createImageList() async {
    await rootBundle.load(widget.imageRoot)
        .then((data) => splitImage(data.buffer.asUint8List()))
        .then((value) => setState(() => imagesData = value));
  }

  List<Image> splitImage(Uint8List input) {
    // convert image to image from image package
    imglib.Image image = imglib.decodeImage(input)!;

    int x = 0, y = 0;
    int width = (image.width / widget.difficulty).floor();
    int height = (image.height / widget.difficulty).floor();

    // split image to parts
    List<imglib.Image> parts = [];
    for (int i = 0; i < widget.difficulty; i++) {
      for (int j = 0; j < widget.difficulty; j++) {
        parts.add(imglib.copyCrop(image, x: x, y: y, width: width, height: height));
        x += width;
      }
      x = 0;
      y += height;
    }

    // convert image from image package to Image Widget to display
    List<Image> output = [];
    for (var img in parts) {
      output.add(Image.memory(imglib.encodeJpg(img)));
    }
    output.insert((widget.difficulty*widget.difficulty-1), Image.asset('assets/images/whiteSquare.jpg'));

    return output;
  }

  createGame(int difficulty) async {
    if (newGame) {
      teasingGame = [];
      for(var i = 0; i < (difficulty*difficulty+1); i++) {
        teasingGame.add(TeasingObject(i, imagesData[i]));
        if (i == difficulty*difficulty-1) { teasingGame.shuffle(); }
      }

      if (!checkSolution(teasingGame)) {
        createGame(difficulty);
      } else {
        newGame = false;
        finish = false;
      }
    }
  }

  bool checkSolution(List<TeasingObject> game) {

    late int evenOdd;

    List<int> solution = [];
    for(var i = 0; i < game.length-1; i++) { //create an complete teasing game (0,1,2,3,4,5,6,7,8) for example
      solution.add(i);
    }

    List<int> actualGame = [];
    for(var i = 0; i < game.length-1; i++) { //create a representation of the teasing game (6,1,8,0,3,4,5,2,7) for example
      actualGame.add(game[i].position);
      if (game[i].position == game.length-2) { evenOdd = i%2; } //Check if empty case is on an odd or even position
    }

    int count = 0;

    // Check how many movement are necessary to have an order list
    //
    // Example :
    // (6,1,8,0,3,4,5,2,7) => start position
    // (6,1,7,0,3,4,5,2,8) => 1 movement
    // (6,1,2,0,3,4,5,7,8) => 2 movements
    // (5,1,2,0,3,4,6,7,8) => 3 movements
    // (4,1,2,0,3,5,6,7,8) => 4 movements
    // (3,1,2,0,4,5,6,7,8) => 5 movements
    // (0,1,2,3,4,5,6,7,8) => 6 movements
    //
    // Her it's odd
    int x = game.length-2;
    while (listEquals(actualGame, solution)) {
      int index = actualGame.indexOf(x);
      if (index != x) {
        actualGame[index] = actualGame[x];
        actualGame[x] = x;
        count++;
      }
      x--;
    }

    //If the count as the same type of the empty case position, the game is possible. Else we need to shuffle again.
    if (count%2 == evenOdd) {
      return true;
    } else {
      return false;
    }
  }

  selectImage(int index) {
    if (finish) {return;}

    bool firstRow = (index >=0 && index < widget.difficulty);
    bool lastRow = (index >= ((widget.difficulty*widget.difficulty)-widget.difficulty) && index < (widget.difficulty*widget.difficulty));
    bool firstColumn = (index%widget.difficulty == 0);
    bool lastColumn = (index%widget.difficulty == widget.difficulty-1);
    int emptyCase = widget.difficulty*widget.difficulty-1;

    if (firstRow) {
      if (firstColumn) {
        if (teasingGame[index+1].position == emptyCase) {return switchImages(index, index+1);}
        else if (teasingGame[index+widget.difficulty].position == emptyCase) {return switchImages(index, index+widget.difficulty);}
      } else if (lastColumn) {
        if (teasingGame[index-1].position == emptyCase) {return switchImages(index, index-1);}
        else if (teasingGame[index+widget.difficulty].position == emptyCase) {return switchImages(index, index+widget.difficulty);}
      } else {
        if (teasingGame[index+1].position == emptyCase) {return switchImages(index, index+1);}
        else if (teasingGame[index-1].position == emptyCase) {return switchImages(index, index-1);}
        else if (teasingGame[index+widget.difficulty].position == emptyCase) {return switchImages(index, index+widget.difficulty);}
      }
    }  else if (lastRow) {
      if (firstColumn) {
        if (teasingGame[index+1].position == emptyCase) {return switchImages(index, index+1);}
        else if (teasingGame[index-widget.difficulty].position == emptyCase) {return switchImages(index, index-widget.difficulty);}
      } else if (lastColumn) {
        if (teasingGame[index-1].position == emptyCase) {return switchImages(index, index-1);}
        else if (teasingGame[index-widget.difficulty].position == emptyCase) {return switchImages(index, index-widget.difficulty);}
      } else {
        if (teasingGame[index+1].position == emptyCase) {return switchImages(index, index+1);}
        else if (teasingGame[index-1].position == emptyCase) {return switchImages(index, index-1);}
        else if (teasingGame[index-widget.difficulty].position == emptyCase) {return switchImages(index, index-widget.difficulty);}
      }
    } else if (firstColumn) {
      if (teasingGame[index+1].position == emptyCase) {return switchImages(index, index+1);}
      else if (teasingGame[index-widget.difficulty].position == emptyCase) {return switchImages(index, index-widget.difficulty);}
      else if (teasingGame[index+widget.difficulty].position == emptyCase) {return switchImages(index, index+widget.difficulty);}
    } else if (lastColumn) {
      if (teasingGame[index-1].position == emptyCase) {return switchImages(index, index-1);}
      else if (teasingGame[index-widget.difficulty].position == emptyCase) {return switchImages(index, index-widget.difficulty);}
      else if (teasingGame[index+widget.difficulty].position == emptyCase) {return switchImages(index, index+widget.difficulty);}
    } else {
      if (teasingGame[index-1].position == emptyCase) {return switchImages(index, index-1);}
      else if (teasingGame[index+1].position == emptyCase) {return switchImages(index, index+1);}
      else if (teasingGame[index-widget.difficulty].position == emptyCase) {return switchImages(index, index-widget.difficulty);}
      else if (teasingGame[index+widget.difficulty].position == emptyCase) {return switchImages(index, index+widget.difficulty);}
    }
  }

  switchImages(int image1, int image2) {
    TeasingObject first = teasingGame[image1];
    TeasingObject second = teasingGame[image2];

    setState(() {
      teasingGame[image1] = second;
      teasingGame[image2] = first;
    });

    checkSuccess();
  }

  checkSuccess() {
    bool success = true;
    int index = 0;
    for (var teasingObject in teasingGame) {
      if(teasingObject.position != index) { success = false; }
      index++;
    }
    if (success) {
      setState(() {
        teasingGame.removeAt(widget.difficulty*widget.difficulty-1);
        finish = true;
      });
    }
  }
}

class TeasingObject {

  int position;
  Image image;

  TeasingObject(this.position, this.image);
}