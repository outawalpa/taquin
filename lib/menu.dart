import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';

class Menu extends StatefulWidget {
  const Menu({super.key});

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blue,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () => Navigator.popAndPushNamed(context, '/storyMode'),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: Image.asset('assets/images/b_story_mode.jpg'),
                  ),
                  Center(
                    child: BorderedText(
                      strokeWidth: 5,
                      child: Text('Story mode'.toUpperCase(),
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
                ],
              )
            ),
            InkWell(
                onTap: () => Navigator.popAndPushNamed(context, '/freeMode'),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: Image.asset('assets/images/b_free_mode.jpg'),
                    ),
                    Center(
                      child: BorderedText(
                        strokeWidth: 5,
                        child: Text('Free mode'.toUpperCase(),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  ],
                )
            ),
            InkWell(
                onTap: () => Navigator.popAndPushNamed(context, '/menu'),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: Image.asset('assets/images/b_speed_mode.jpg'),
                    ),
                    Center(
                      child: BorderedText(
                        strokeWidth: 5,
                        child: Text('Speed mode'.toUpperCase(),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}