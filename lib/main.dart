import 'package:flutter/material.dart';
import 'package:animator/animator.dart';
import 'package:flutter/services.dart';
import 'package:taquin/free_mode.dart';
import 'package:taquin/story_mode.dart';

import 'menu.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Taquin',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/home',
      routes: {
        '/home': (context) => const MyHomePage(),
        '/menu': (context) => const Menu(),
        '/storyMode': (context) => const StoryMode(),
        '/freeMode': (context) => const FreeMode(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () => Navigator.popAndPushNamed(context, '/menu'),
        child: Container(
          color: Colors.blue,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: SizedBox(
                  height: 250,
                  child: Animator<double>(
                    tween: Tween<double>(begin: 150, end: 200),
                    cycles: 0,
                    duration: const Duration(seconds: 2),
                    builder: (context, animatorState, child) => Center(
                      child: Container(
                        margin: const EdgeInsets.all(20),
                        height: animatorState.value,
                        width: animatorState.value,
                        child: Image.asset('assets/images/home.png'),
                      ),
                    ),
                  ),
                ),
              ),
              Text('Click to continue'.toUpperCase(),
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
