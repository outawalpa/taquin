import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';

import 'const/const.dart';

class StoryMode extends StatefulWidget {
  const StoryMode({super.key});

  @override
  State<StoryMode> createState() => _StoryModeState();
}

class _StoryModeState extends State<StoryMode> {

  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GestureDetector(
          onHorizontalDragEnd: (details){
            if (details.primaryVelocity! < 0 && index < storiesTitle.length-1) {
              setState(() {
                index += 1;
              });
            }
            if(details.primaryVelocity! > 0 && index > 0) {
              setState(() {
                index -= 1;
              });
            }
          },
          onTap: () {

          },
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Image.asset(storiesBackground[index], fit: BoxFit.fitHeight,),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: BorderedText(
                        strokeWidth: 5,
                        child: Text(storiesTitle[index],
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Container(
                        height: 300,
                        alignment: Alignment.center,
                        child: ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: storiesTitle.length,
                            itemBuilder: (context, x) {
                              return Stack(
                                alignment: Alignment.center,
                                children: [
                                  Center(
                                    child: Icon(
                                      Icons.circle,
                                      color: Colors.black,
                                      size: x == index ? 50 : 30,
                                    ),
                                  ),
                                  Center(
                                    child: Icon(
                                      Icons.circle,
                                      color: Colors.white,
                                      size: x == index ? 40 : 20,
                                    ),
                                  )
                                ],
                              );

                            }
                        )
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}